import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const FavoriteItem = props => {
  const {brand, description, createdAt, id, image, model, name, price} =
    props.product;
  return (
    <TouchableOpacity style={styles.container}>
      <View>
        {image ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              style={{width: 100, height: 100}}
              source={{
                uri: image,
              }}
              resizeMode="cover"
            />
          </View>
        ) : null}
        <View
          style={{
            paddingTop: 20,
            height: 50,
          }}>
          <Text style={{color: 'black'}} numberOfLines={1}>
            {name}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 15,
          }}>
          <Text style={{fontSize: 18, color: 'black', fontWeight: 'bold'}}>
            {price?.toString() + ' ₺'}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default FavoriteItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    margin: 10,
    padding: 10,
    width: '100%',
    height: 250,
    alignSelf: 'center',
    justifyContent:"center",
    alignItems:"center",
    borderRadius: 6,
    borderWidth: 0.4,
    borderColor: 'grey',
  },
});
