import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import React, {useState, useContext} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';

const ProductItem = props => {
  const {brand, description, createdAt, id, image, model, name, price} =
    props.product;
  const {state, dispatchToContext} = useContext(Context);
  const [isFavorite, setIsFavorite] = useState();

  const handleAddFavorite = async () => {
    setIsFavorite(favorite => !favorite);
    props.addFavorite(props.product);
  };

  const handleRemoveFavorite = async () => {
    setIsFavorite(favorite => !favorite);
    props.removeFavorite(props.product);
  };

  return (
    <TouchableOpacity
      onPress={props.onPressProductDetail}
      style={styles.container}>
      <View>
        {image ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              style={{width: 100, height: 100}}
              source={{
                uri: image,
              }}
              resizeMode="contain"
            />
          </View>
        ) : null}
        <View
          style={{
            paddingTop: 20,
            height: 50,
          }}>
          <Text style={{color: 'black'}} numberOfLines={1}>
            {name}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 15,
          }}>
          <Text style={{fontSize: 18, color: 'black', fontWeight: 'bold'}}>
            {price?.toString() + ' ₺'}
          </Text>
        </View>
      </View>
      <View style={styles.bottom}>
        <TouchableOpacity onPress={props.onAddToCart} style={styles.signIn}>
          <LinearGradient
            colors={['#275bf6', '#275bf6']}
            style={[
              styles.signIn,
              {
                marginTop: Platform.OS === 'android' ? 20 : 20,
              },
            ]}>
            <Text style={[styles.textSign, {color: '#fff'}]}>Add To Cart</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      {isFavorite === true ? (
        <TouchableOpacity
          onPress={handleRemoveFavorite}
          style={styles.favorite}>
          <Ionicons name="star" color="orange" size={25} />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity onPress={handleAddFavorite} style={styles.favorite}>
          <Ionicons name="star-outline" color="black" size={25} />
        </TouchableOpacity>
      )}
    </TouchableOpacity>
  );
};

export default ProductItem;

const styles = StyleSheet.create({
  container: {
    flex: 0.5,
    flexDirection: 'column',
    margin: 10,
    padding: 10,
    width: '100%',
    height: 300,
    alignSelf: 'center',
    borderRadius: 6,
    borderWidth: 0.4,
    borderColor: 'grey'
  },
  signIn: {
    width: '100%',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    bottom: 0,
    marginTop: 20,
  },
  textSign: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 25,
    paddingTop: 10,
  },
  favorite: {
    position: 'absolute',
    right: 5,
    top: 5,
  },
});
