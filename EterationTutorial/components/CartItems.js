import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const CartItems = props => {
  return (
    <View style={styles.listItem}>
      <View
        style={{
          alignItems: 'flex-start',
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View style={{flex: 1}}>
          <Text
            adjustsFontSizeToFit
            numberOfLines={2}
            style={{color: 'grey', fontSize: 16}}>
            {props.items.name}
          </Text>
        </View>
        <View style={{flex: 0.7}}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 16,
              color: '#275bf6',
            }}>
            {(
              Number(props.items.count) * Number(props.items.price)
            ).toString() + ' ₺ '}
          </Text>
        </View>
        <View style={styles.countContainer}>
          <TouchableOpacity
            style={styles.countButtonMinus}
            onPress={props.MinusItem}>
            <Text style={{color: '#275bf6', fontSize: 30}}>-</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: '#275bf6',
              fontSize: 26,
              borderRadius: 4,
              padding: 20,
            }}>
            <Text style={styles.countText}>{props.items.count}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.countButtonMinus}
            onPress={props.PlusItem}>
            <Text style={{fontSize: 30, color: '#275bf6'}}>+</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default CartItems;

const styles = StyleSheet.create({
  listItem: {
    margin: 10,
    padding: 20,
    backgroundColor: '#f5f5f5',
    width: '90%',
    flex: 1,
    alignSelf: 'center',
    flexDirection: 'column',
    borderRadius: 20,
    shadowOffset: {
      width: 2,
      height: 4,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowColor: 'grey',
  },
  countContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    borderColor: 'grey',
  },
  countButtonMinus: {
    backgroundColor: 'lightgrey',
    padding: 7,
    borderRadius: 4,
  },
  countText: {
    fontWeight: 'bold',
    color: 'white',
  },
});
