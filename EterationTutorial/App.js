import {NavigationContainer} from '@react-navigation/native';
import React, {useReducer} from 'react';
import {StatusBar} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {badgeCount} from './context/reducers/badgeCount';
import {cart} from './context/reducers/cart';
import {favorite} from './context/reducers/favorite';
import {combineReducers} from './context/reducers/index';
import Context, {initialState} from './context/store';
import MainNavigation from './navigation/MainNavigation';

Ionicons.loadFont();

const appReducers = combineReducers({
  cart: cart,
  badgeCount: badgeCount,
  favorite: favorite,
});

export default function App() {
  const [state, dispatchToContext] = useReducer(appReducers, initialState);
  return (
    <Context.Provider value={{state, dispatchToContext}}>
      <NavigationContainer>
        <StatusBar barStyle="light-content" backgroundColor="#275bf6" />
        <MainNavigation />
      </NavigationContainer>
    </Context.Provider>
  );
}
