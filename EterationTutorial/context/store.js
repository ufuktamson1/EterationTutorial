import {createContext} from 'react';

export const initialState = {
  cart: [],
  badgeCount: 0,
  favorite:[]
};

export default Context = createContext(initialState);
