import AsyncStorage from '@react-native-async-storage/async-storage';

export function favorite(state, action) {
  switch (action.type) {
    case 'ADDFAVORITE':
      let objIndex = state.favorite.findIndex(obj => obj.id == action.value.id);
      if (objIndex < 0) {
        state.favorite = [...state.favorite, action.value];
      }
      setStorage(state.favorite);
      return {...state};
    case 'REMOVEFAVORITE':
      let objIndex2 = state.favorite.findIndex(
        obj => obj.id == action.value.id,
      );
      if (objIndex2 >= 0) {
        state.favorite.splice(objIndex2, 1);
      }
      setStorage(state.favorite);
      return {...state};
    case 'CLEARFAVORITE':
      state.favorite = [];
      setStorage([]);
      return {...state};
    case 'GETFAVORITE':
      if (action.value) {
        state.favorite = action.value;
      }
      return {...state};
    default:
      return state;
  }
}

const setStorage = async item => {
  await AsyncStorage.setItem('favorites', JSON.stringify(item));
};
