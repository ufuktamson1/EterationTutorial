import AsyncStorage from "@react-native-async-storage/async-storage";

export function cart(state, action) {
  switch (action.type) {
    case "ADD":
      let objIndex = state.cart?.findIndex((obj) => obj.id == action.value.id);
      if (objIndex >= 0) {
        state.cart[objIndex].count += 1;
      } else {
        action.value.count = 1;
        state.cart = [...state.cart, action.value];
      }
      setStorage(state.cart);
      return { ...state };
    case "REMOVE":
      let objIndex2 = state.cart.findIndex((obj) => obj.id == action.value.id);
      if (objIndex2 >= 0) {
        if (state.cart[objIndex2].count > 0) {
          state.cart[objIndex2].count -= 1;
        }
        if (state.cart[objIndex2].count === 0) {
          state.cart.splice(objIndex2, 1);
        }
      }
      setStorage(state.cart);
      return { ...state };
    case "CLEAR":
      state.cart = [];
      setStorage([]);
      return { ...state };
    case "GETCART":
      if (action.value) {
        state.cart = action.value;
      }
      return { ...state };
    default:
      return state;
  }
}

const setStorage = async (item) => {
  await AsyncStorage.setItem("items", JSON.stringify(item));
};
