import AsyncStorage from "@react-native-async-storage/async-storage";

export function badgeCount(state, action) {
  switch (action.type) {
    case "ADDBADGE":
      state.badgeCount += 1;
      setStorage(state.badgeCount);
      return { ...state };
    case "REMOVEBADGE":
      state.badgeCount -= 1;
      setStorage(state.badgeCount);
      return { ...state };
    case "CLEARBADGE":
      state.badgeCount = 0;
      setStorage(0);
      return { ...state };
    case "GETBADGE":
      if (action.value && action.value > 0) {
        state.badgeCount = action.value;
      }
      return { ...state };
    default:
      return state;
  }
}

const setStorage = async (item) => {
  await AsyncStorage.setItem("itemsBadge", JSON.stringify(item));
};