import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {useContext} from 'react';
import {useTheme} from 'react-native-paper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Context from '../context/store';
import CartScreen from '../screens/CartScreen';
import Favorites from '../screens/Favorites';
import ProductDetail from '../screens/ProductDetail';
import ProductList from '../screens/ProductList';

const Tab = createMaterialBottomTabNavigator();
const ProductStack = createStackNavigator();
const CartStack = createStackNavigator();
const FavoritesStack = createStackNavigator();

const MainNavigation = () => {
  const {state, dispatchToContext} = useContext(Context);
  const theme = useTheme();
  theme.colors.secondaryContainer = 'transperent';

  return (
    <Tab.Navigator
      activeColor="black"
      inactiveColor="grey"
      shifting={true}
      initialRouteName="ProductList"
      barStyle={{
        position: 'absolute',
        elevation: 0,
        backgroundColor: '#fff',
        height: 80,
        paddingTop: 0,
      }}>
      <Tab.Screen
        name="ProductList"
        component={ProductStackScreen}
        options={{
          tabBarLabel: 'Anasayfa',
          tabBarIcon: ({color}) => (
            <Ionicons name="home-outline" color={color} size={25} />
          ),
        }}
      />
      <Tab.Screen
        name="Cart"
        component={CartStackScreen}
        options={{
          tabBarBadge: state.badgeCount === 0 ? null : state.badgeCount,
          tabBarLabel: 'Sepetim',
          tabBarIcon: ({color}) => (
            <Ionicons name="cart-outline" color={color} size={25} />
          ),
        }}
      />
      <Tab.Screen
        name="Favorite"
        component={FavoritesStackScreen}
        options={{
          tabBarLabel: 'Favoriler',
          tabBarIcon: ({color}) => (
            <Ionicons name="star-outline" color={color} size={25} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const ProductStackScreen = ({navigation}) => {
  return (
    <ProductStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#275bf6',
          height: 100,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: '600',
        },
      }}>
      <ProductStack.Screen
        name="ProductListStack"
        component={ProductList}
        options={{
          title: 'Ürün Listesi',
        }}
      />
      <ProductStack.Screen
        name="ProductDetail"
        component={ProductDetail}
        options={{
          title: 'Ürün Detayı',
        }}
      />
    </ProductStack.Navigator>
  );
};

const CartStackScreen = ({navigation}) => {
  return (
    <CartStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#275bf6',
          height: 100,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: '600',
        },
      }}>
      <CartStack.Screen
        name="CartStack"
        component={CartScreen}
        options={{
          title: 'Sepetim',
        }}
      />
    </CartStack.Navigator>
  );
};

const FavoritesStackScreen = ({navigation}) => {
  return (
    <FavoritesStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#275bf6',
          height: 100,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: '600',
        },
      }}>
      <FavoritesStack.Screen
        name="FavoriteStack"
        component={Favorites}
        options={{
          title: 'Favoriler',
        }}
      />
    </FavoritesStack.Navigator>
  );
};

export default MainNavigation;
