import AsyncStorage from '@react-native-async-storage/async-storage';
import cuid from 'cuid';
import debounce from 'lodash.debounce';
import React, {useContext, useEffect, useMemo, useRef, useState} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {Modalize} from 'react-native-modalize';
import ProductItem from '../components/ProductItem';
import axiosInstance from '../config/axiosInstance';
import Context from '../context/store';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {RadioButton} from 'react-native-paper';

const itemsPerLoad = 12;

const ProductList = ({route, navigation}) => {
  const [productData, setproductData] = useState([]);
  const [data, setData] = useState([]);
  const [filteredProductData, setFilteredProductData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const {state, dispatchToContext} = useContext(Context);
  const modalizeRef = useRef();
  const [loadedItems, setLoadedItems] = useState(0);
  const [searchQuery, setSearchQuery] = useState(0);
  const [value, setValue] = React.useState('high');

  useEffect(() => {
    getStoredProducts();
  }, []);

  useEffect(() => {
    getProducts();
  }, []);

  const getStoredProducts = async () => {
    let items = await AsyncStorage.getItem('items');
    let itemsBadge = await AsyncStorage.getItem('itemsBadge');
    let parsedItems = JSON.parse(items);
    let parsedItemsBadge = JSON.parse(itemsBadge);
    await dispatchToContext({type: 'GETCART', value: parsedItems});
    await dispatchToContext({type: 'GETBADGE', value: parsedItemsBadge});
  };

  const getProducts = async () => {
    try {
      const res = await axiosInstance.get();
      setproductData(res.data);
      setFilteredProductData(res.data);
      console.log(res.data);
      loadInitialData(res.data);
      setIsLoading(false);
    } catch (error) {
      console.log('error', error);
      setIsLoading(false);
    }
  };

  const loadInitialData = allData => {
    const initialData = allData.slice(0, itemsPerLoad);
    setData(initialData);
    setLoadedItems(itemsPerLoad);
  };

  const loadMoreData = () => {
    if (isLoadMore || searchQuery) {
      return;
    }
    setIsLoadMore(true);

    const newData = productData.slice(loadedItems, loadedItems + itemsPerLoad);
    setLoadedItems(loadedItems + itemsPerLoad);

    setTimeout(() => {
      setData([...data, ...newData]);
      setIsLoadMore(false);
    }, 1000);
  };

  const handleNavigateProductDetail = item => {
    navigation.navigate('ProductDetail', {productItem: item});
  };

  const handleAddToCart = async item => {
    await dispatchToContext({type: 'ADD', value: item});
    await dispatchToContext({type: 'ADDBADGE'});
  };

  const addFavorite = async item => {
    let items = await AsyncStorage.getItem('favorites');
    let parsedItems = JSON.parse(items);
    let objIndex = parsedItems.findIndex(obj => obj.id == item.id);
    if (objIndex < 0) {
      parsedItems.push(item);
    }
    await AsyncStorage.setItem('favorites', JSON.stringify(parsedItems));
  };

  const removeFavorite = async item => {
    let items = await AsyncStorage.getItem('favorites');
    let parsedItems = JSON.parse(items);
    let objIndex = parsedItems.findIndex(obj => obj.id == item.id);
    if (objIndex >= 0) {
      parsedItems.splice(objIndex, 1);
    }
    await AsyncStorage.setItem('favorites', JSON.stringify(parsedItems));
  };

  const renderListItem = itemData => {
    return (
      <ProductItem
        key={cuid()}
        onPressProductDetail={() => handleNavigateProductDetail(itemData.item)}
        product={itemData.item}
        onAddToCart={() => handleAddToCart(itemData.item)}
        addFavorite={() => addFavorite(itemData.item)}
        removeFavorite={() => removeFavorite(itemData.item)}
      />
    );
  };

  const handleFilterSearch = t => {
    setSearchQuery(t);
    if (t?.length > 0) {
      console.log('product data:', filteredProductData);
      const searchedData = filteredProductData.filter(p =>
        p.name.toUpperCase().includes(t.toUpperCase()),
      );
      console.log('Filtrelenmiş Veriler:', searchedData);

      setData(searchedData);
      setLoadedItems(itemsPerLoad);
    } else {
      const initialData = productData.slice(0, itemsPerLoad);
      setData(initialData);
      setLoadedItems(itemsPerLoad);
    }
  };

  const renderFooter = () => {
    if (!isLoadMore) return null;

    return (
      <View
        style={{
          marginBottom: 100,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator
          style={{marginTop: 10}}
          size="large"
          color="#275bf6"
        />
      </View>
    );
  };

  const handleFilterSave = () => {
    let data = productData;
    let sortedData;
    if ((value === 'high')) {
      sortedData = data.sort((a, b) => Number(b.price) - Number(a.price));
    } else {
      sortedData = data.sort((a, b) => Number(a.price) - Number(b.price));
    }
    setData(sortedData);
    modalizeRef.current.close();
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator
            style={{marginTop: 10}}
            size="large"
            color="#275bf6"
          />
        </View>
      ) : (
        <View style={{flex: 1}}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <TextInput
              onChangeText={text => handleFilterSearch(text)}
              placeholder="Search..."
              placeholderTextColor={'grey'}
              style={styles.textInput}></TextInput>
          </View>
          <View
            style={{
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
              margin: 8,
            }}>
            <Text>Filters:</Text>
            <TouchableOpacity
              onPress={() => {
                modalizeRef.current?.open();
              }}
              style={styles.signIn}>
              <Text style={[styles.textSign, {color: 'black'}]}>
                Select Filters
              </Text>
            </TouchableOpacity>
          </View>
          <FlatList
            style={{flex: 1}}
            data={data}
            renderItem={renderListItem}
            extraData={data}
            keyExtractor={(item, index) => index.toString()}
            removeClippedSubviews={true}
            numColumns={2}
            onEndReached={loadMoreData}
            onEndReachedThreshold={0.1}
            ListFooterComponent={renderFooter}
            showsVerticalScrollIndicator={false}
          />
        </View>
      )}
      <Modalize
        adjustToContentHeight={true}
        modalStyle={{marginBottom: 55, backgroundColor: 'white'}}
        snapPoint={300}
        ref={modalizeRef}>
        <ScrollView style={{height: Dimensions.get('window').height * 0.75}}>
          <TouchableOpacity
            onPress={() => modalizeRef.current.close()}
            style={styles.cancel}>
            <Ionicons
              style={{padding: 5}}
              name="close-outline"
              color="black"
              size={35}
            />
          </TouchableOpacity>
          <View style={styles.modalView}>
            <Text style={{fontSize: 18, fontWeight: '400'}}>Price</Text>
            <RadioButton.Group
              onValueChange={value => setValue(value)}
              value={value}>
              <RadioButton.Item label="High to low" value="high" />
              <RadioButton.Item label="Low to high" value="low" />
            </RadioButton.Group>
          </View>

          <TouchableOpacity onPress={handleFilterSave} style={styles.signIn2}>
            <Text style={[styles.textSign2, {color: '#fff'}]}>Save</Text>
          </TouchableOpacity>
        </ScrollView>
      </Modalize>
    </View>
  );
};

export default ProductList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textInput: {
    width: '95%',
    height: 50,
    padding: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    margin: 10,
  },
  signIn: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: 'lightgrey',
    borderRadius: 5,
  },
  textSign: {
    fontSize: 16,
    fontWeight: '400',
    padding: 10,
  },
  modalView: {
    width: '100%',
    height: 200,
    backgroundColor: '#fff',
    margin: 5,
  },
  loading: {
    marginBottom: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cancel: {
    left: 5,
    top: 5,
    backgroundColor: '#f5f5f5',
    borderRadius: 10,
    width: 45,
    height: 45,
    marginBottom: 10,
  },
  signIn2: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: '#275bf6',
    padding: 5,
    margin: 10,
  },
  textSign2: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingHorizontal: 20,
  },
});
