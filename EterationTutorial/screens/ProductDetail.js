import AsyncStorage from '@react-native-async-storage/async-storage';
import {useIsFocused} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Context from '../context/store';
import Ionicons from 'react-native-vector-icons/Ionicons';

const ProductDetail = ({route, navigation}) => {
  const {productItem} = route.params;
  const {state, dispatchToContext} = useContext(Context);
  const [isFavorite, setIsFavorite] = useState(false);
  const isFocused = useIsFocused();

  useEffect(() => {
    if (isFocused) {
      handleIsFavorite();
    }
  }, [isFocused,isFavorite]);

  const handleIsFavorite = async () => {
    let items = await AsyncStorage.getItem('favorites');
    let parsedItems = JSON.parse(items);
    let objIndex = parsedItems.findIndex(obj => obj.id == productItem.id);
    if (objIndex >= 0) {
      setIsFavorite(true);
    } else {
      setIsFavorite(false);
    }
  };

  const handleAddToCart = async () => {
    await dispatchToContext({type: 'ADD', value: productItem});
    await dispatchToContext({type: 'ADDBADGE'});
  };

  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <Image
          resizeMode="cover"
          source={{uri: productItem.image}}
          style={styles.carouselimage}
        />
        <Text style={styles.title}>{productItem.name}</Text>
        <Text style={styles.description}>{productItem.description}</Text>
      </ScrollView>
      <View style={styles.bottom}>
        <View
          style={{
            flexDirection: 'row',
            marginBottom: 10,
            justifyContent: 'space-between',
            flex: 1,
          }}>
          <View>
            <Text style={[styles.textSign2, {color: '#275bf6'}]}>
              Total
              {' :'}
            </Text>
            <Text style={[styles.textSign2, {color: 'black'}]}>
              {productItem.price.toString() + ' ₺'}
            </Text>
          </View>
        </View>
        <TouchableOpacity onPress={handleAddToCart} style={styles.signIn}>
          <Text style={[styles.textSign, {color: '#fff'}]}>Add To Cart</Text>
        </TouchableOpacity>
      </View>
      {isFavorite === true ? (
        <View style={styles.favorite}>
          <Ionicons name="star" color="orange" size={30} />
        </View>
      ) : (
        <View style={styles.favorite}>
          <Ionicons name="star-outline" color="white" size={30} />
        </View>
      )}
    </View>
  );
};

export default ProductDetail;

const styles = StyleSheet.create({
  carouselimage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height / 2.5,
  },
  title: {
    fontSize: 32,
    margin: 10,
    fontWeight: '600',
  },
  description: {
    fontSize: 14,
    margin: 10,
    color: 'grey',
  },
  bottom: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: '#fff',
    paddingBottom: 10,
    borderTopWidth: 2,
    borderTopColor: '#dddddd',
    height: '10%',
    width: '100%',
    backgroundColor: '#fff',
    marginBottom: 80,
    opacity: 0.9,
  },
  signIn: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: '#275bf6',
    padding: 5,
    right: 20,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingHorizontal: 20,
  },
  textSign2: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 20,
  },
  favorite: {
    position: 'absolute',
    right: 15,
    top: 15,
  },
});
