import {useIsFocused} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import CartItems from '../components/CartItems';
import Context from '../context/store';

const CartScreen = () => {
  const {state, dispatchToContext} = useContext(Context);
  const [cartItems, setCartItems] = useState(state.cart);
  const [totalItemPrice, setTotal] = useState(0);
  const isFocused = useIsFocused();

  useEffect(() => {
    if (isFocused) {
      setCartItems(state.cart);
      handleCalculatePrice();
    }
  }, [isFocused]);

  const handlePlusItem = async item => {
    await dispatchToContext({type: 'ADD', value: item});
    await dispatchToContext({type: 'ADDBADGE'});
    handleCalculatePrice();
  };
  const handleMinusItem = async item => {
    await dispatchToContext({type: 'REMOVE', value: item});
    await dispatchToContext({type: 'REMOVEBADGE'});
    handleCalculatePrice();
  };

  const handleCalculatePrice = () => {
    let totalPrice = 0;
    for (let i = 0; i < state.cart.length; i++) {
      let total = state.cart[i]?.count * state.cart[i]?.price;
      totalPrice = total + totalPrice;
    }
    setTotal(totalPrice);
  };

  const renderListItem = itemData => {
    return (
      <CartItems
        items={itemData.item}
        PlusItem={() => handlePlusItem(itemData.item)}
        MinusItem={() => handleMinusItem(itemData.item)}
      />
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        style={{flex: 1}}
        data={cartItems}
        renderItem={renderListItem}
        keyExtractor={(item, index) => index.toString()}
        extraData={cartItems}
        ListFooterComponent={<View style={styles.listFooter}></View>}
      />

      <View style={styles.bottom}>
        <View
          style={{
            flexDirection: 'row',
            marginBottom: 10,
            justifyContent: 'space-between',
            flex: 1,
          }}>
          <View>
            <Text style={[styles.textSign2, {color: '#275bf6'}]}>
              Total
              {' :'}
            </Text>
            <Text style={[styles.textSign2, {color: 'black'}]}>
              {totalItemPrice.toString() + ' ₺'}
            </Text>
          </View>
        </View>
        <TouchableOpacity style={styles.signIn}>
          <Text style={[styles.textSign, {color: '#fff'}]}>Complete</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CartScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0,
  },
  listFooter: {
    marginBottom: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: '#fff',
    paddingBottom: 10,
    borderTopWidth: 2,
    borderTopColor: '#dddddd',
    height: '10%',
    width: '100%',
    backgroundColor: '#fff',
    marginBottom: 80,
    opacity: 0.9,
  },
  signIn: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: '#275bf6',
    padding: 5,
    right: 20,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingHorizontal: 20,
  },
  textSign2: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 20,
  },
});
