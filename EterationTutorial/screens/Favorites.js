import AsyncStorage from '@react-native-async-storage/async-storage';
import {useIsFocused} from '@react-navigation/native';
import cuid from 'cuid';
import React, {useContext, useEffect, useState} from 'react';
import {ActivityIndicator, FlatList, StyleSheet, View} from 'react-native';
import FavoriteItem from '../components/FavoriteItem';
import Context from '../context/store';

const Favorites = () => {
  const [productData, setproductData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const isFocused = useIsFocused();

  useEffect(() => {
    if (isFocused) {
      getFavorites();
    }
  }, [isFocused]);

  const getFavorites = async () => {
    let items = await AsyncStorage.getItem('favorites');
    let parsedItems = JSON.parse(items);
    setproductData(parsedItems);
    setIsLoading(false);
  };

  const renderListItem = itemData => {
    return <FavoriteItem key={cuid()} product={itemData.item} />;
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator
            style={{marginTop: 10}}
            size="large"
            color="#275bf6"
          />
        </View>
      ) : (
        <View style={{flex: 1}}>
          <FlatList
            style={{flex: 1}}
            data={productData}
            renderItem={renderListItem}
            keyExtractor={(item, index) => index.toString()}
            removeClippedSubviews={true}
            initialNumToRender={12}
            ListFooterComponent={
              <View
                style={{
                  marginBottom: 100,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}></View>
            }
            showsVerticalScrollIndicator={false}
          />
        </View>
      )}
    </View>
  );
};

export default Favorites;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
