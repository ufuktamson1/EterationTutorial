import React from 'react';
import renderer from 'react-test-renderer';
import ProductList from '../screens/ProductList';

test('renders successfully', () => {
  const tree = renderer.create(<ProductList />).toJSON();
  expect(tree).toMatchSnapshot();
});


test('example test with AsyncStorage', async () => {
  await AsyncStorage.setItem('key', 'value');
  const retrievedValue = await AsyncStorage.getItem('key');
  
  expect(retrievedValue).toBe('value');
});