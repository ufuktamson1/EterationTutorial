import React from 'react';
import renderer from 'react-test-renderer';
import ProductDetail from '../screens/ProductDetail';

test('renders successfully', () => {
  const tree = renderer.create(<ProductDetail />).toJSON();
  expect(tree).toMatchSnapshot();
});