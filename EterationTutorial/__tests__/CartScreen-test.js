import React from 'react';
import renderer from 'react-test-renderer';
import CartScreen from '../screens/CartScreen';

test('renders successfully', () => {
  const tree = renderer.create(<CartScreen />).toJSON();
  expect(tree).toMatchSnapshot();
});
