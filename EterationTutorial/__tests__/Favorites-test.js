import React from 'react';
import renderer from 'react-test-renderer';
import Favorites from '../screens/Favorites';
import AsyncStorage from '@react-native-async-storage/async-storage';

test('renders successfully', () => {
  const tree = renderer.create(<Favorites />).toJSON();
  expect(tree).toMatchSnapshot();
});

test('example test with AsyncStorage', async () => {
  await AsyncStorage.setItem('key', 'value');
  const retrievedValue = await AsyncStorage.getItem('key');
  
  expect(retrievedValue).toBe('value');
});